#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>

#pragma pack(1)

//int = 4
//short = 2
//char = 1

typedef struct BMPHeader {
    short formatIdentifier; //19778 = bitmap
    int sizeOfFile;
    short reserved1;
    short reserved2;
    int pixelArrayOffset;
} BMPHeader;

typedef struct DIBHeader {
    int sizeOfDIBHeader;
    int widthOfImage;
    int heightOfImage;
    short colourPlanes;
    short bitsPerPixel;
    int compressionScheme;
    int imageSize;
    int horizontalResolution;
    int verticalResolution;
    int coloursInPalette;
    int importantColours;
} DIBHeader;

typedef struct Pixel {
    char blue;
    char green;
    char red;
} Pixel;

int main(int argc, char** argv) {
    const int BITMAP_FORMAT = 19778;
    FILE* filePointer;
    if(argc < 3) {
        printf("Not enough parameters");
        return 0;
    }
    char* bitMapFileName = argv[2];
    char* operation = argv[1];
    int paddingBytes;
    filePointer = fopen(bitMapFileName, "rb+");
    BMPHeader bitMapHeader;
    DIBHeader dibHeader;
    fread(&bitMapHeader, sizeof(bitMapHeader), 1, filePointer);
    if(bitMapHeader.formatIdentifier != 19778) {
        printf("This isn't a valid bmp file.\r\n\r\n");
        return 0;
    }
    fread(&dibHeader, sizeof(dibHeader), 1, filePointer);
    if(dibHeader.sizeOfDIBHeader != 40) {
        printf("Invalid size of DIB header");
        return 0;
    }
    if(dibHeader.bitsPerPixel != 24) {
        printf("Invalid size of DIB header");
        return 0;
    }
    fseek(filePointer, bitMapHeader.pixelArrayOffset, SEEK_SET);
    paddingBytes = ((dibHeader.widthOfImage * 3) % 4) * 3;
    for(int i = 0; i < dibHeader.heightOfImage; i++) {
        for(int j = 0; j < dibHeader.widthOfImage; j++) {
            Pixel currentPixel;
            fread(&currentPixel, sizeof(currentPixel), 1, filePointer);
            fseek(filePointer, -3, SEEK_CUR);
            if(strcmp(operation, "--invert") == 0) {
                currentPixel.blue = ~currentPixel.blue;
                currentPixel.red = ~currentPixel.red;
                currentPixel.green = ~currentPixel.green;
            } else if (strcmp(operation, "--greyscale") == 0) {
                double blue = (double) currentPixel.blue / 255.;
                double green = (double) currentPixel.green / 255.;
                double red = (double) currentPixel.red / 255.;
                double gamma;
                double y = (0.2126 * red)  + (0.7152 * green) + (0.0722 * blue);
                if(y <= 0.0031308) {
                    gamma = 12.92 * y;
                } else {
                    gamma = pow(y, 1/2.4);
                    gamma *= 1.055;
                    gamma -= 0.055;
                }
                gamma *= 255;
                currentPixel.red = (char) gamma;
                currentPixel.blue = (char) gamma;
                currentPixel.green = (char) gamma;
            }
            fwrite(&currentPixel, sizeof(currentPixel), 1, filePointer);
        }
        fseek(filePointer, paddingBytes, SEEK_CUR);
    }
    return 0;
}